<?php return array(
    //主题名
    'name' => 'Volunteer',
    //别名名
    'title' => '志愿者主题',
    //版本号
    'version' => '1.0.0',
    //是否商业模版,1是，0，否
    'is_com' => 0,
    //模版描述
    'summary' => '寿光志愿者官网',
    //开发者
    'developer' => '山东大叶榕信息科技有限公司',
    //开发者网站
    'website' => 'http://www.ibanyan.net',
);