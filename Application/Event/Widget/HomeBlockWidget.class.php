<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 15-4-30
 * Time: 下午1:28
 * @author 郑钟良<zzl@ourstu.com>
 */

namespace Event\Widget;


use Think\Controller;

class HomeBlockWidget extends Controller
{
    public function render()
    {
        $this->assignEvent();
        import_lang('Event');
        $this->display(T('Application://Event@Widget/homeblock'));
    }
	
    private function assignEvent()
    {
        $title = modC('EVENT_SHOW_TITLE', '', 'Event');//标题
        $num = modC('EVENT_SHOW_COUNT', 6, 'Event');//首页展示个数
        $type = modC('EVENT_SHOW_TYPE', 0, 'Event');//删选类型，1为后台推荐，0为全部
        $field = modC('EVENT_SHOW_ORDER_FIELD', 'view_count', 'Event');//排序方式
        $order = modC('EVENT_SHOW_ORDER_TYPE', 'desc', 'Event');//活动查找升序降序
        $cache = modC('EVENT_SHOW_CACHE_TIME', 600, 'Event');//缓存时间
        $do = S('event_home_do');
		$doing = S('event_home_doing');
		$did = S('event_home_did');
        if (!$do) {
        	$order = $field . " " . $order;
            $where['sTime'] = array('lt',time());
            $where['deadline'] = array('egt',time());
			$do = $this-> _get_list($where,$order,$num);
            S('event_home_do', $do, $cache);
        }
       	//进行中
        if (!$doing) {
            $order = $field . " " . $order;
            $where['eTime'] = array('egt',time());
            $where['deadline'] = array('lt',time());
            $doing = $this-> _get_list($where,$order,$num);
            S('event_home_doing', $doing, $cache);
        }
        //已结束
        if (!$did) {
            $order = $field . " " . $order;
            $where['eTime'] = array('lt',time());
            $did = $this-> _get_list($where,$order,$num);
            S('event_home_did', $did, $cache);
        }
//dump($list);exit;
        $this->assign('event_lists_do', $do);
    	$this->assign('event_lists_doing',$doing);
    	$this->assign('event_lists_did',$did);
    }
    
    /*
     * 获取进行中、招募中、已结束的活动
     */
    protected function _get_list($where,$order,$num){	
            if ($type == 0) {
            	 $list = M('Event')->where(array('status' => 1))->where($where)->limit($num)->order($order)->select();
    	    } else {
                $list = M('Event')->where(array('status' => 1, 'is_recommend' => 1))->where($where)->limit($num)->order($order)->select();
            }
            foreach ($list as &$v) {
//              $v['user'] = query_user(array('space_url', 'nickname'), $v['uid']);
                $v['user'] = query_user(array('id', 'username', 'nickname', 'space_url', 'space_link', 'avatar128', 'rank_html'), $v['uid']);
	           // $v['type'] = $this->getType($v['type_id']);
	            $v['check_isSign'] = D('event_attend')->where(array('uid' => is_login(), 'event_id' => $v['id']))->select();
	            $v['group'] = D('Group')->where(array('uid' => $v['uid']))->find();
            }
            unset($v);
            return $list;
	}
} 