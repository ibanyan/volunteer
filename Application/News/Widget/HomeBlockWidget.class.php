<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 15-4-30
 * Time: 下午1:28
 * @author 郑钟良<zzl@ourstu.com>
 */

namespace News\Widget;


use Think\Controller;

class HomeBlockWidget extends Controller
{
    public function render()
    {
        $this->assignNews();
        import_lang('News');
        $this->display(T('Application://News@Widget/homeblock'));
    }

    private function assignNews()
    {
        $num = modC('NEWS_SHOW_COUNT', 6, 'News');
        $type = modC('NEWS_SHOW_TYPE', 0, 'News');
        $field = modC('NEWS_SHOW_ORDER_FIELD', 'view', 'News');
        $order = modC('NEWS_SHOW_ORDER_TYPE', 'desc', 'News');
        $cache = modC('NEWS_SHOW_CACHE_TIME', 600, 'News');
        $list_announce = S('news_home_announce');
        $list_mien = S('news_home_mien');
        if (!$list_announce) {
        	$category = 1;
        	$list_announce = $this->_get_news_list($num,$field,$order,$category);//公告
            S('news_home_announce', $list_announce, $cache);
        }
        if(!$list_mien){
        	$category = 2;
        	$list_mien = $this->_get_news_list($num,$field,$order,$category);
        	S('news_home_mien',$list_mien,$cache);
        }
        $this->assign('news_lists_announce', $list_announce);
        $this->assign('news_lists_mien',$list_mien);
    }
    
    private function _get_news_list($num,$field,$order,$category){
    	
            if ($type) {
                /**
                 * 获取推荐位数据列表
                 * @param  number $pos 推荐位 1-系统首页，2-推荐阅读，4-本类推荐
                 * @param  number $category 分类ID
                 * @param  number $limit 列表行数
                 * @param  boolean $filed 查询字段
                 * @param order 排序
                 * @return array             数据列表
                 */
                $list = D('News/News')->position(1, null, $num, true, $field . ' ' . $order);
            } else {
                $map = array('status' => 1, 'category'=> $category ,'dead_line' => array('gt', time()));
                $list = D('News/News')->getList($map, $field . ' ' . $order, $num);
            }
            foreach ($list as &$v) {
                $v['user'] = query_user(array('space_url', 'nickname'), $v['uid']);
            }
            unset($v);
            if (!$list) {
                $list = 1;
            }
            return $list;
    }
} 